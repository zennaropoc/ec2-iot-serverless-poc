'use strict';
const AWS = require('aws-sdk')
const fs = require('fs')
const config = JSON.parse(fs.readFileSync(__dirname+'/config.json').toString())
const athena = new AWS.Athena({region:config.region});

module.exports.start = () => {
    return new Promise((resolve, reject) => {
        const hours = 4 * 60 * 60 * 1000
        const time = new Date().getTime() - hours
        var params = {
            QueryString: 'SELECT * FROM datalake WHERE timestamp > ' + time + ' order by device,timestamp', /* required */
            ResultConfiguration: { /* required */
                OutputLocation: 's3://' + config.result_bucket + '/results/', /* required */
                EncryptionConfiguration: {
                    EncryptionOption: "SSE_S3"
                }
            },
            QueryExecutionContext: {
                Database: 'ec2_sls_poc'
            }
        };
        console.log(JSON.stringify(params))
        athena.startQueryExecution(params, function (err, data) {
            if (err) {
                const result = {
                    error: true,
                    message: err
                }
                reject(result)
            }
            else {
                const result = {
                    error: false,
                    message: data
                }
                resolve(result)
            }
        });
    })

}
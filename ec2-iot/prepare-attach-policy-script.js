const fs = require("fs")
const policy = JSON.parse(fs.readFileSync("policy-arn.json").toString())
const certificates = JSON.parse(fs.readFileSync("certificates.json").toString())
const command = "aws iot attach-principal-policy --policy-name "+policy.policyName+" --principal "+certificates.certificateArn+" --region eu-west-1"
fs.writeFileSync("./attach-policy.sh",command)
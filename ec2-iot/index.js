'use strict'
const awsIot = require('aws-iot-device-sdk')
const iotNamespace = 'ec2-serverless-iot-poc'
const WebSocket = require('ws');
const clientId = 'ec2-websocket-iot-bridge'
const fs = require("fs")
const iotconf = JSON.parse(fs.readFileSync("iotendpoint.json").toString())
const athena = require("./athena-handler")

var device = awsIot.device({
    keyPath: 'private.pem.key',
    certPath: 'certificate.pem.crt',
    caPath: 'verisign.pem',
    clientId: clientId,
    host: iotconf.endpointAddress
});
const wss = new WebSocket.Server({ port: 8080 });
var globalws, interval0, interval1, interval2

wss.on('connection', function connection(ws) {
    globalws = ws
    globalws.on('message', function incoming(message) {
        const parsed = JSON.parse(message)
        console.log(parsed)
        try {
            switch (parsed.source) {
                case "webconsole":
                    athena.start().then((res) => {
                        globalws.send(JSON.stringify(
                            {
                                source: "bridge",
                                message: "Athena query started: " + JSON.stringify(res)
                            }))
                    }).catch((err) => {
                        globalws.send(JSON.stringify(
                            {
                                source: "bridge",
                                message: "Athena query error: " + JSON.stringify(err)
                            }))
                    })
                    console.log('query started', parsed.message)
                    break;
                default: break;
            }
        } catch (e) {
            console.log(e)
        }

    });

    globalws.send(JSON.stringify({source: "bridge", message: "Connected to the EC2/IoT Bridge via Websocket"}));
});

device
    .on('connect', function () {
        console.log('Device ' + clientId + ' connected');

        device.subscribe(iotNamespace + "/broadcast");
        device.subscribe(iotNamespace + "/" + clientId);
        interval0 = setInterval(() => {
            device.publish(iotNamespace + "/logs", JSON.stringify(getPayload('device_0')));
        }, 500)
        interval1 = setInterval(() => {
            device.publish(iotNamespace + "/logs", JSON.stringify(getPayload('device_1')));
        }, 500)
        interval2 = setInterval(() => {
            device.publish(iotNamespace + "/logs", JSON.stringify(getPayload('device_2')));
        }, 500)

    });

device
    .on('message', function (topic, payload) {
        console.log('message', topic, payload.toString());
        const data = JSON.parse(payload.toString())
        try {
            globalws.send(JSON.stringify(
                {
                    source: "iot",
                    message: payload.toString()
                }))

        } catch (e) {
            console.log(e)
        }

    });

device
    .on('error', function (err) {
        console.log(err)
    })

function getPayload(id) {
    return {
        device: id,
        temperature: Math.floor(Math.random() * 10) + 4,
        humidity: Math.floor(Math.random() * 50) + 40,
        timestamp: new Date().getTime()
    }
}






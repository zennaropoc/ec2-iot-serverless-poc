#!/bin/bash
mkdir certs
cd certs
aws iot create-keys-and-certificate --set-as-active --certificate-pem-outfile cert.pem --public-key-outfile public.key --private-key-outfile private.key
cd ..

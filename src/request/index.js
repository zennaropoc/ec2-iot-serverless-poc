'use strict';
var AWS = require('aws-sdk')
var athena = new AWS.Athena();
const iotdata = new AWS.IotData({ endpoint: 'a2w1g7qf8qsjpp.iot.eu-west-1.amazonaws.com' });

var iotParams = {
    topic: 'ec2-serverless-iot-poc/broadcast', /* required */
    payload: ""
}

module.exports = (event, context, callback) => {
    const hours = 4 * 60 * 60 * 1000
    const time = new Date().getTime() - hours
    var params = {
        QueryString:  'SELECT * FROM "ec2_sls_poc"."datalake" WHERE timestamp > '+time+' order by device,timestamp', /* required */
        ResultConfiguration: { /* required */
            OutputLocation: 's3://' + process.env.RESULT_BUCKET + '/results/', /* required */
            EncryptionConfiguration: {
                EncryptionOption: "SSE_S3"
            }
        },
        QueryExecutionContext: {
            Database: 'ec2_sls_poc'
        }
    };
    console.log(JSON.stringify(params))
    athena.startQueryExecution(params, function (err, data) {
        if (err){
            const result = {
                error : true,
                message : err
            }
            iotParams.payload = JSON.stringify(result)
            iotdata.publish(iotParams, function (err, data) {
                return callback(null, err); // an error occurred
            });
        } 
        else {
            const result = {
                error : false,
                message : data,
                type: "started"
            }
            iotParams.payload = JSON.stringify(result)
            iotdata.publish(iotParams, function (err, data) {
                return callback(null, data)
            });
            
        }
    });
}
/*
module.exports = (event, context, callback) => {
  const hours = 4 * 60 * 60 * 1000
  const time = new Date().getTime() - hours
 
  var params = {
    QueryString: query, 
    ResultConfiguration: { 
      OutputLocation: 's3://'+process.env.RESULT_BUCKET+'/'
    },
    QueryExecutionContext: {
      Database: 'ec2_sls_poc'
    }
  };
  console.log(query)
  athena.startQueryExecution(params, function(err, data) {
    if (err){
      const errorResponse = {
        statusCode: 500,
        body: JSON.stringify({
          message: 'Go Serverless v1.0! Your function executed successfully!',
          input: event,
          error: err
        }),
      };
      console.log(JSON.stringify(err))
      callback(null, errorResponse);
    } 
    else {
      //TODO: generazione signed url
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Go Serverless v1.0! Your function executed successfully!',
          input: event
        }),
      };
      console.log(JSON.stringify(data))
      callback(null, response);
    }    
  });
  

 
};
*/
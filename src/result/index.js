'use strict';
const AWS = require('aws-sdk')
const s3 = new AWS.S3()
const fs = require('fs')
const iotConf = JSON.parse(fs.readFileSync(__dirname+'/iot.json').toString())
const iotdata = new AWS.IotData({ endpoint: iotConf.endpointAddress });
var iotParams = {
  topic: 'ec2-serverless-iot-poc/broadcast',
  payload: ""
}

function getSignedUrl(bucket, key) {
  return new Promise((resolve, reject) => {
    try {
      var params = {
        Bucket: bucket,
        Key: key
      };
      s3.getSignedUrl('getObject', params, function (err, url) {
        if (err) {
          iotParams.payload = JSON.stringify(Object.assign(err,{fail:true}))
          iotdata.publish(iotParams, function (err2, data) {
            reject(err)
          });

        } else {
          iotParams.payload = JSON.stringify({fail:false,url:url})
          iotdata.publish(iotParams, function (err, data) {
            resolve(url)
          });
        }
      });
    } catch (e) {
      reject(e)
    }
  })

}


module.exports = (event, context, callback) => {
  console.log(JSON.stringify(event))
  if(event.Records[0].s3.object.key.indexOf('.metadata') == -1){
    getSignedUrl(event.Records[0].s3.bucket.name, event.Records[0].s3.object.key).then((url) => {
    const successResponse = {
      statusCode: 200,
      body: JSON.stringify({
        source: 'lambda',
        message: url
      }),
    };
    console.log(JSON.stringify(successResponse))
    callback(null, successResponse);
  }).catch((err) => {
    const failResponse = {
      statusCode: 500,
      body: JSON.stringify({
        source: 'lambda',
        message: err
      }),
    };

    callback(null, failResponse);
  })
  } else {
    callback(null);
  }
  

};
'use strict'

module.exports = {
  request : require("./src/request"),
  result : require("./src/result")
}
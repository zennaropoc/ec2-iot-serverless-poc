
First of all, I apologize for using the term EC2 and Serverless in the same project name!

# What this POC does?

Create a cloudformation stack with serverless.com framework which creates:

 - An Ubuntu EC2 instance with bootstrap actions (definded on userdata_template.sh). This EC2 is used for www dashboard and also for websocket server which works as a bridge between IoT and web dashboard, and also runs 3 virtual IoT devices that publish messages to IoT endpoint
 - EC2 security group to allow traffic from 80 and 8080 (where websocket is listening)
 - IoT certificates (downloaded iside EC2)
 - IoT policy associated to the certificates
 - IoT rules (which forwards /log topic to Kinesis Firehose and /command topic to a Lambda)
 - Kinesis Firehose -> S3 (datalake)
 - IoT rule to Firehose (Select * FROM 'ec2-serverless-iot/logs') to save into datalake iot devices data
 - Athena Database and Table over S3 datalake
 - S3 bucket for Athena query results
 - A Lambda triggered when a new object is created on the Athena results bucket, this Lambda get the key created, generates a SignedUrl and publishes the SignedUrl to the /broadcast IoT topic. The EC2 forwards messages received from IoT /broadcast topic to the websocket.
 - Roles and policies for Lambda and Kinesis

The www dashboard allows to receive messages from IoT (through EC2 websocket) and to send a command (over websocket) which launches an Athena query, to get last 4 hours of data published from IoT virtual devices to the datalake. When Athena ends, the result is created to the S3 result bucket and at the same time a Lambda get the result filename (key), generates the SignedUrl, and send the link to the web console (via EC2 websocket)

![Architecture-flow](./architecture-flow.png)

## Prerequisites

- After cloning the repository, you should replace in the package.json all the occurrence of **ACCOUNTID** with your AWS Account ID
- Generate a Key Pair called **serverless-poc** from AWS Webconsole (EC2 section), it is associated to the EC2 on the Cloudformation script (and you can use it to connect to the instance)

## How to deploy with CI/CD

Project includes a bitbucket-pipeline.yml which allows (if you have pipelines activated on your bitbucket repository) to automaticaly execute on every push:

 - launch a docker image with aws cli and node
 - install serverless on that docker image
 - execute tests
 - run predeploy command (which generates userdata.sh script from **userdata_template.sh**, athena tables generation scripts, and other config)
 - run deploy command (which creates artifact and deploy the cloudformation stack and Lambda on AWS)
 - run postdeploy command (which sync S3 script buckets with /ec2-iot and /ec2-www folders, used from the EC2 to get the scripts and creates Athena database/table)

It requires to set some AWS variables on Bitbucket Pipelines Environment Variables:

 - AWS_ACCESS_KEY_ID
 - AWS_DEFAULT_REGION
 - AWS_SECRET_ACCESS_KEY

Look at **bitbucket-pipelines.yml** to see the deploy pipeline

## Deploy from local machine

Clone repository, install node and launch:

    npm install serverless -g
    npm run deploy

## How to remove
    
 - Stop EC2 instance without terminate it (to stop pushing data to IoT -> Firehose -> S3, because the "remove" script will empty the bucket first)
 - Launch:npm run remove

## What you should see (after deploy)?
 - The Bitbucket pipeline running

![Pipeline](./pipeline.png)

 - All resources created on Cloudformation stack **ec2-serverless-iot-poc-dev**

![Cloudformation](./cloudformation-stack.png)

 - Datalake bucket should receive stream from kinesis

![Datalake](./s3-datalake.png)

 - The website dashboard visible on the EC2 public ip address

![Dashboard](./dashboard.png)

 - The Athena results links when push the "Request" button

![Athena](./athena.png)


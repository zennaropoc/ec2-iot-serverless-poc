CREATE SCHEMA IF NOT EXISTS ec2_sls_poc
COMMENT 'Ec2 Serverless Poc Athena Database'
LOCATION 's3://ec2-serverless-iot-poc-datalake-563555401602/'
WITH DBPROPERTIES ('creator'='Simone Zennaro')
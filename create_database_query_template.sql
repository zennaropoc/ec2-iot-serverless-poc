CREATE SCHEMA IF NOT EXISTS ec2_sls_poc
COMMENT 'Ec2 Serverless Poc Athena Database'
LOCATION 's3://ec2-serverless-iot-poc-datalake-%ACCOUNTID%/'
WITH DBPROPERTIES ('creator'='Simone Zennaro')
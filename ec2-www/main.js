var wshost = 'ws://'+window.location.hostname+':8080', socket;

function handleResponse(event){
    var data = JSON.parse(event.data)
    switch(data.source){
        case "iot" : 
            var jsonMessage = JSON.parse(data.message)
            $('#result').append('<a href="'+jsonMessage.url+'" target="_blank">Download Athena Result</a><br/>')
            break;
        case "bridge" : 
        $('#result').append('<div>'+data.message+'</div>')
            break;
        default : 
            $('#result').append('<div>'+JSON.stringify(data)+'</div>')
    }
}

$(document).ready(() => {
    socket = new WebSocket(wshost);
    socket.onmessage = function(event){
        console.log(event)
        handleResponse(event)
    }
    $('#daily-log-request').click(() => {
        socket.send(JSON.stringify({
            source : "webconsole",
            message: "daily-log-request"
        }))
    })
})
#!/bin/bash
sudo apt-get update
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get -y install apache2
sudo systemctl restart apache2
sudo apt-get -y install awscli
sudo printf "start on startup\ntask\nexec /home/ubuntu/ec2-iot/iotbridge.sh" > /etc/init/wiot_bridge.conf
mkdir /var/www/html/dashboard/
mkdir /home/ubuntu/ec2-iot/
cd /home/ubuntu/ec2-iot/
wget https://www.symantec.com/content/en/us/enterprise/verisign/roots/VeriSign-Class%203-Public-Primary-Certification-Authority-G5.pem -O verisign.pem
printf '{\n  "Version": "2012-10-17",\n  "Statement": [\n    {\n      "Action": [\n        "iot:*"\n      ],\n      "Resource": [\n        "*"\n      ],\n      "Effect": "Allow"\n    }\n  ]\n}' > policy.json
printf 'const fs = require("fs")\nconst policy = JSON.parse(fs.readFileSync("policy-arn.json").toString())\nconst certificates = JSON.parse(fs.readFileSync("certificates.json").toString())\nconst command = "aws iot attach-principal-policy --policy-name "+policy.policyName+" --principal "+certificates.certificateArn+" --region eu-west-1"\nfs.writeFileSync("./attach-policy.sh",command)' > prepare-attach-policy-script.js
aws iot create-keys-and-certificate --set-as-active --certificate-pem-outfile certificate.pem.crt --public-key-outfile public.key --private-key-outfile private.pem.key --region eu-west-1 > certificates.json
aws iot create-policy --policy-name ec2-serverless-poc --policy-document file://policy.json --region eu-west-1 > policy-arn.json
nohup sh -c 'sleep 10s && aws s3 sync s3://ec2-serverless-iot-poc-ec2-scripts-563555401602/ec2-www/ /var/www/html/ && aws s3 sync s3://ec2-serverless-iot-poc-ec2-scripts-563555401602/ec2-iot/ /home/ubuntu/ec2-iot/ && nodejs prepare-attach-policy-script.js && sh attach-policy.sh && npm install && aws iot describe-endpoint --region eu-west-1 > iotendpoint.json && chown -R ubuntu:ubuntu /home/ubuntu/ec2-iot && npm install pm2 -g && pm2 start /home/ubuntu/ec2-iot/index.js --name ec2iot -o /home/ubuntu/ec2-iot/pm2.log && pm2 startup > startup-sh && ./startup.sh' &
#http://pm2.keymetrics.io/docs/usage/quick-start/




'use strict'
const fs = require('fs')

function getParamValue(param) {
    let result = null
    process.argv.forEach(function (elem, index) {
        if (elem.indexOf(param) > -1) result = process.argv[index].split("=")[1]
    })
    return result
}

var userdata = fs.readFileSync('./userdata_template.sh').toString()
userdata = userdata.replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account')).replace(new RegExp('\%REGION\%','g'),getParamValue('region'))
fs.writeFileSync('./userdata.sh',userdata)

var athena_database = fs.readFileSync('./create_database_query_template.sql').toString()
athena_database = athena_database.replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account'))
fs.writeFileSync('./create_database_query.sql',athena_database)

var athena_table = fs.readFileSync('./create_table_query_template.sql').toString()
athena_table = athena_table.replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account'))
fs.writeFileSync('./create_table_query.sql',athena_table)

var athena_database_script = "aws athena start-query-execution --query-string file://create_database_query.sql --result-configuration OutputLocation=s3://ec2-serverless-iot-poc-athena-result-%ACCOUNTID%/logs/ --region %REGION% --output text > athena.log".replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account')).replace(new RegExp('\%REGION\%','g'),getParamValue('region'))
fs.writeFileSync('./athena_database.sh',athena_database_script)

var athena_table_script = "aws athena start-query-execution --query-string file://create_table_query.sql --result-configuration OutputLocation=s3://ec2-serverless-iot-poc-athena-result-%ACCOUNTID%/logs/ --region %REGION% --output text".replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account')).replace(new RegExp('\%REGION\%','g'),getParamValue('region'))
fs.writeFileSync('./athena_table.sh',athena_table_script)

var delete_policy = "aws iot delete-policy --policy-name ec2-serverless-poc --region %REGION% \nexit 0".replace(new RegExp('\%REGION\%','g'),getParamValue('region'))
fs.writeFileSync('./delete_policy.sh',delete_policy)

var empty_bucket = "aws s3 rm s3://ec2-serverless-iot-poc-athena-result-%ACCOUNTID%/ --recursive && aws s3 rm s3://ec2-serverless-iot-poc-ec2-scripts-%ACCOUNTID%/ --recursive && aws s3 rm s3://ec2-serverless-iot-poc-datalake-%ACCOUNTID%/ --recursive \nexit 0".replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account'))
fs.writeFileSync('./empty-buckets.sh',empty_bucket)

var iot_endpoint = "aws iot describe-endpoint --region %REGION% > ./src/result/iot.json".replace(new RegExp('\%REGION\%','g'),getParamValue('region'))
fs.writeFileSync('./iot_endpoint.sh',iot_endpoint)

var athenaConfig = '{"region":"%REGION%", "result_bucket" : "ec2-serverless-iot-poc-athena-result-%ACCOUNTID%"}'.replace(new RegExp('\%ACCOUNTID\%','g'),getParamValue('account')).replace(new RegExp('\%REGION\%','g'),getParamValue('region'))
fs.writeFileSync('./ec2-iot/athena-handler/config.json',athenaConfig)
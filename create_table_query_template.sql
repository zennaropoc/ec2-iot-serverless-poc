CREATE EXTERNAL TABLE `ec2_sls_poc.datalake`(
  `device` string COMMENT 'from deserializer', 
  `temperature` int COMMENT 'from deserializer', 
  `humidity` int COMMENT 'from deserializer',
  `timestamp` bigint COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.openx.data.jsonserde.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://ec2-serverless-iot-poc-datalake-%ACCOUNTID%/'
TBLPROPERTIES (
  'has_encrypted_data'='false', 
  'transient_lastDdlTime'='1526827116')